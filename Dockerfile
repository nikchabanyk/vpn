FROM alpine:latest

RUN apk add --no-cache strongswan

CMD ["ipsec", "start", "--nofork", "--auto-update", "10"]